from PIL import Image
import sys, subprocess, os, string, shutil

import ocr_gui # GUI

# set containing possible targets
target_file = set(line.strip().lower().translate(None, string.punctuation).decode('unicode_escape').encode('ascii','ignore') for line in open('./target.txt').read().split())

def identify_target(filename, temp='./temp', dest='./screenshots_txt'):
    global target_file

    # run tesseract
    # subprocess.call(['tesseract', temp+'/'+filename, dest+'/'+filename, '--psm', '11', 'quiet'])
    subprocess.call(['tesseract', temp+'/'+filename, dest+'/'+filename, 'quiet'])

    # get file content and clean up
    # change spaces to new line, trim spaces, change to lower case, remove punctuation, ignore unicode characters
    file1 = dest+'/'+filename+'.txt'
    file1_content = list(line.strip().lower().translate(None, string.punctuation).decode('unicode_escape').encode('ascii','ignore') for line in open(file1).read().split())

    # compare content with file_target
    matches = set() # to store matches between target_file and screenshot text
    for word in file1_content:
        if word in target_file:
            matches.add(word)

    if len(matches)==1: return next(iter(matches)) # match found

    # no matches found, crop image and try again
    img = Image.open(temp+'/'+filename)
    img.crop((0, 0, img.size[0], img.size[1]/3)).save(temp+'/'+filename)

    # subprocess.call(['tesseract', temp+'/'+filename, dest+'/'+filename, '--psm', '11', 'quiet'])
    subprocess.call(['tesseract', temp+'/'+filename, dest+'/'+filename, 'quiet'])
    file1_content = list(line.strip().lower().translate(None, string.punctuation).decode('unicode_escape').encode('ascii','ignore') for line in open(file1).read().split())

    matches = set() # to store matches between target_file and screenshot text
    for word in file1_content:
        if word in target_file:
            matches.add(word)

    if len(matches)==1: return next(iter(matches)) # match found

    return "unknown" # give up

def main(src='./screenshots', dest='./screenshots_txt'):
    # check/make directories
    if not os.path.exists(src):
        print 'input folder not found'
        exit()
    if not os.path.exists('./temp'):
        os.makedirs('./temp')
    if not os.path.exists(dest):
        os.makedirs(dest)

    # loop through screenshots
    for filename in os.listdir(src):
        if filename.startswith('.') or os.path.isdir(filename): continue # skip hidden files

        print 'processing: '+filename,

        try:
            # open image and save to temp folder
            img = Image.open(src+'/'+filename)
            img.save('./temp/'+filename)

            # run ocr and identify target
            target = identify_target(filename, './temp', dest)
            print ' -> target: '+target

            # rename file
            os.rename(dest+'/'+filename+'.txt', dest+'/'+filename+'->'+target+'.txt');
        except IOError:
            print ' -> not an image'

    # remove temp directory
    shutil.rmtree('./temp')

    print 'done!'

if __name__ == '__main__':
    if len(sys.argv)>1 and sys.argv[1]=='-h': # help is on its way
        print 'usage: '+os.path.basename(__file__)+' [ -c ] [ input directory ] [ output directory]\n'
        print '-c\trun without gui'
        print '-h\tshow this help'
    elif len(sys.argv)>1 and sys.argv[1]=='-c': # a command line guy huh
        if len(sys.argv)==4:
            main(sys.argv[2], sys.argv[3]) # run with user given directories
        else:
            main() # run with default directories
            print 'no directories given... running with default'
    else:
        ocr_gui.mainGUI()
