# GUI
import Tkinter as tk
from random import randint
import tkFileDialog, os, subprocess, sys, threading, tkMessageBox

from ocr_identity_targets import main # main function

srcdir="./screenshots"
outdir="./screenshots_txt"
root = tk.Tk()

# open dialog for input directory
def sourceDir():
    global srcdir, srcdir_tk, root

    srcdir = tkFileDialog.askdirectory(parent=root, initialdir=srcdir, title='Please select input file.')
    root.title(srcdir)
    srcdir_tk.set(srcdir)

# open dialog for output directory
def outputDir():
    global outdir, outdir_tk, root

    outdir = tkFileDialog.askdirectory(parent=root, initialdir=outdir, title='Please select output directory.')
    root.title(outdir)
    outdir_tk.set(outdir)

# create new thread to run ocr (ref ocr_identity_targets.py)
def run_ocr_identify_target():
    global srcdir, outdir

    t = threading.Thread(target=main, args=(srcdir, outdir))
    t.daemon = True
    print 'starting...'
    t.start()

def ask_quit():
    if tkMessageBox.askokcancel("Quit", "No! I'm not ready to say goodbye..."):
        root.destroy()

# redirect stdout to text widget
class StdoutRedirector(object):
    def __init__(self,text_widget):
        self.text_space = text_widget

    def write(self,string):
        self.text_space.insert('end', string)
        self.text_space.see('end')

# main method for gui, this should run first
def mainGUI():
    global srcdir, srcdir_tk, outdir, outdir_tk, submit, text, root

    # create window
    # root.geometry("600x400")
    root.geometry('%dx%d+%d+%d' % (600, 400, 100, 100))
    root.title("OCR Target Identifier")

    # directory frame
    directoryLabelFrame = tk.LabelFrame(root, text="Select directories: ")
    directoryLabelFrame.pack()

    # directory -> source / input
    sourceFrame = tk.LabelFrame(directoryLabelFrame)
    sourceFrame.pack()
    srcdir_tk = tk.StringVar(sourceFrame)
    srcdir_tk.set(os.getcwd()) # set default to current directory

    sourcedirectory = tk.Label(sourceFrame, text="Select source directory:")
    sourcedirectory.pack(side="left") # directory display

    inFileTxt = tk.Entry(sourceFrame, textvariable=srcdir_tk)
    inFileTxt.pack(side="left") # directory string

    browse1 = tk.Button(sourceFrame, text="Browse ...", command = sourceDir)
    browse1.pack() # browse button

    # directory -> destination / output
    outputFrame = tk.LabelFrame(directoryLabelFrame)
    outputFrame.pack()
    outdir_tk = tk.StringVar()
    outdir_tk.set(os.getcwd()) # set default to current directory

    outputdirectory = tk.Label(outputFrame, text="Select output directory:")
    outputdirectory.pack(side="left") # directory display

    outFileTxt = tk.Entry(outputFrame, textvariable=outdir_tk)
    outFileTxt.pack(side="left") # directory string

    browse2 = tk.Button(outputFrame, text="Browse ...", command = outputDir)
    browse2.pack() # browse button

    # submit button
    submit = tk.Button(directoryLabelFrame, text="Begin", command = run_ocr_identify_target)
    submit.pack()

    # text widget / terminal output
    text = tk.Text(root)
    text.pack()
    sys.stdout = StdoutRedirector(text)

    # quit confirmation
    root.protocol("WM_DELETE_WINDOW", ask_quit)

    # [for fun] greeting message
    greetings=["yo mama so fat, I took a photo of her last Christmas and it's still printing", "yo mama so fat, before God said 'Let there be light', he told her to move out of the way", "yo mama so fat, when she walked past the TV, I missed 3 seasons of Friends", "yo momma so fat, when she fell, no one was laughing but the ground was cracking up", "yo momma so fat, her bellybutton gets home 15 minutes before she does", "yo mamma so fat she doesn't need the internet - she's already world wide"]
    print greetings[(randint(0,len(greetings)-1))]

    # open main window
    root.mainloop()
