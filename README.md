## 1. install tesseract##

Ubuntu: ```sudo apt-get install tesseract-ocr```
    
Mac OS: ```brew install tesseract```
    
Windows: Get a real computer,
        [or click here](https://github.com/tesseract-ocr/tesseract/wiki/Downloads)

## 2. Run the program##

usage: ocr_identity_targets.py [ options ] [ input directory ] [ output directory]

-c	run without gui
-h	show this help

By default the program will look in ./screenshots/ for inputs and ./screenshots_txt/ for outputs